﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityController : MonoBehaviour
{
    [Header("Entity")]
    public LayerMask targetMask;
    public float health;
    public float healthMax;
    public float healthRegen;
    public float healthRegenDelay;
    public float impactDamage;
    public float flashSpeed;
    public float flashDuration;
    [HideInInspector] public float railSpeed;
    public ParticleSystem fxExplosion;
    public float xPosLimit;
    public float yPosLimit;

    protected Collider _collider;
    protected MeshRenderer _mesh;
    protected Rigidbody _rb;
    protected Color _originalMeshColor;
    protected float _originalRailSpeed;
    protected bool _initialized;
    protected bool _flashing;
    protected bool _dead;

    private float _prevHealth;
    private float _timeOfLastDamageTaken;

    protected void Start()
    {
        health = healthMax;
        _prevHealth = health = healthMax;
        _collider = GetComponent<Collider>();
        _mesh = GetComponent<MeshRenderer>();
        if (_mesh != null && _mesh.material != null)
        {
            _originalMeshColor = _mesh.material.color;
        }
        _rb = GetComponent<Rigidbody>();
        _originalRailSpeed = railSpeed;
    }

    protected void Update()
    {
        if (health > 0)
        {
            // Health regenerates after a you take damage and a delay has occured
            if (health < _prevHealth)
            {
                _timeOfLastDamageTaken = GameManager.I.clock.totalSeconds;
            }
            else if (health > 0 && health < healthMax && GameManager.I.clock.totalSeconds >= _timeOfLastDamageTaken + healthRegenDelay)
            {
                health = Mathf.Min(health + healthRegen, healthMax);
            }
            _prevHealth = health;
        }
        else if (health <= 0 && !_dead)
        {
            _dead = true;
            _collider.enabled = false;
            _mesh.enabled = false;         
            fxExplosion.Play();
        }
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (((1 << other.gameObject.layer) & targetMask) != 0)
        {
            //Debug.Log(other.name + " collided with me - " + gameObject.name);
            EntityController entity = other.transform.GetComponent<EntityController>();
            if (_initialized && entity != null && entity._initialized)
            {
                entity.health = Mathf.Max(0, entity.health - impactDamage);
                //Debug.Log("entity.health is " + entity.health);
                if (entity.health > 0)
                {
                    entity.Flash(GameManager.I.damageTaken);
                }
            }
        }
    }
    
    public void Flash(Color replacement)
    {
        if (!_flashing)
        {
            StartCoroutine(FlashMaterial(replacement));
        }
    }

    private IEnumerator FlashMaterial(Color replacement)
    {
        _flashing = true;
        float _flashStartTime = GameManager.I.clock.totalSeconds;
        while (GameManager.I.clock.totalSeconds < _flashStartTime + flashDuration)
        {
            _mesh.material.color = replacement;
            yield return new WaitForSeconds(1/flashSpeed);
            _mesh.material.color = _originalMeshColor;
            yield return new WaitForSeconds(1/flashSpeed);
        }
        _flashing = false;
    }
}
