﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    [Header("Projectile")]
    public LayerMask targetMask;
    public float damage;
    public float speed;
    public float duration;
    public float impactDestroyDelay;
    [HideInInspector] public Vector3 direction;
    [HideInInspector] public float startTime;

    protected CapsuleCollider _collider;

    private Rigidbody _rb;

    protected void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _collider = GetComponent<CapsuleCollider>();
        startTime = GameManager.I.clock.totalSeconds;
    }

    protected void FixedUpdate()
    {
        transform.Translate(direction.normalized * speed * GameManager.I.clock.timeScale * Time.deltaTime);
    }

    protected void Update()
    {
        if (GameManager.I.clock.totalSeconds > startTime + duration)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (((1 << other.gameObject.layer) & targetMask) != 0)
        {
            ShipController ship = other.transform.GetComponent<ShipController>();
            AsteroidController asteroid = other.GetComponent<AsteroidController>();
            Explosive explosive = other.GetComponent<Explosive>();
            if (ship != null)
            {
                Debug.Log(name + " collided with " + ship.name);
                if (!ship.evading)
                {
                    ship.health = Mathf.Max(0, ship.health - damage);
                    if (ship.health > 0)
                    {
                        ship.Flash(GameManager.I.damageTaken);
                    }
                    startTime = GameManager.I.clock.totalSeconds;
                    duration = impactDestroyDelay;
                }
                else if (ship.GetComponent<PlayerController>() != null)
                {
                    GameManager.I.clock.SetTimeScale(((PlayerController)ship).avoidBulletTimeScale, ((PlayerController)ship).avoidBulletTimeDuration);
                }
            }
            else if (asteroid != null)
            {
                Debug.Log(name + " collided with " + asteroid.name);
                asteroid.health = Mathf.Max(0, asteroid.health - damage);
                if (asteroid.health > 0)
                {
                    asteroid.Flash(GameManager.I.damageTaken);
                }
                startTime = GameManager.I.clock.totalSeconds;
                duration = impactDestroyDelay;
            }
            else if (explosive != null)
            {
                Debug.Log(name + " collided with " + explosive.name);
                startTime = explosive.startTime = GameManager.I.clock.totalSeconds;
                duration = explosive.duration = impactDestroyDelay;
            }
        }
    }
}
