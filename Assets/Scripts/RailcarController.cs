﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailcarController : MonoBehaviour
{
    [HideInInspector] public EntityController[] entities;
    public float speed;
    public float currentDistance;

    private Vector3 _newPos;
    private float _prevPosTime;

    private void Awake()
    {
        entities = transform.GetComponentsInChildren<EntityController>();
        for (int i=0; i<entities.Length; i++)
        {
            entities[i].railSpeed = speed;
        }
    }

    private void Start()
    {
        _prevPosTime = GameManager.I.clock.totalSeconds;
        transform.position = _newPos = RailcarManager.I.GetPosOnRail(currentDistance);
    }

    private void FixedUpdate()
    {
        // Updates speed for the RailcarManager as a result of boosting/evading
        if (entities[0] != null)
        {
            speed = entities[0].railSpeed;
        }
    }

    private void Update()
    {
        if (transform.position != _newPos)
        {
            transform.position = Vector3.MoveTowards(transform.position, _newPos, Mathf.Abs(speed * 100 * Time.deltaTime));
        }
        else
        {
            currentDistance += ((GameManager.I.clock.totalSeconds - _prevPosTime) * speed);
            _newPos = RailcarManager.I.GetPosOnRail(currentDistance);
            _prevPosTime = GameManager.I.clock.totalSeconds;
            transform.LookAt(_newPos);
        }
    }
}
