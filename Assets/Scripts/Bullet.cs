﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VolumetricLines.Utils;

public class Bullet : Projectile
{
    [Header("Bullet")]
    public float length;
    public float width;
    public Color lineColor;
    public Color glowColor;

    private VolumetricLines.VolumetricLineBehavior _volumetricLine;
    private Light _glow;

    private new void Start ()
    {
        base.Start();
        _volumetricLine = GetComponent<VolumetricLines.VolumetricLineBehavior>();
        _volumetricLine.EndPos = direction * length;
        _volumetricLine.LineWidth = width;
        _volumetricLine.LineColor = lineColor;
        _glow = GetComponent<Light>();
        _glow.color = glowColor;
        _collider = GetComponent<CapsuleCollider>();
        _collider.center = _volumetricLine.EndPos;
        _collider.radius = width / 2;
    }
}
