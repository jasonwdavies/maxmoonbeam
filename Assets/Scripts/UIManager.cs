﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    private static UIManager _I;
    public static UIManager I
    {
        get
        {
            if (_I == null)
            {
                _I = FindObjectOfType<UIManager>();
            }
            return _I;
        }
    }

    [HideInInspector] public bool initialized;

    public GameObject healthUI;
    public GameObject ammoUI;
    public Image vision;
    public Text text;

    private EntityController _player;
    private List<Graphic> _fadingGraphics;
    private Vector3 _scale;

    private void Start()
    {
        _player = GameManager.I.playerRailcar.entities[0];
        _fadingGraphics = new List<Graphic>();
        _scale = Vector3.one;
        //GameManager.I.clock.Pause();
        // TODO: Pause until after the fade is complete!
        ApplyFade(text, Color.clear, Color.black, GameManager.I.sceneFadeTime/2, true);
        ApplyFade(vision, Color.white, Color.clear, GameManager.I.sceneFadeTime, false);
        //GameManager.I.clock.Unpause();
        // TODO: Should mve this into Update()
        StartCoroutine(UpdateUI());
    }

    private void UpdateHealthUI()
    {
        if (_player != null)
        {
            _scale.x = _player.health / _player.healthMax;
            _scale.y = _scale.z = 1;
            healthUI.transform.localScale = _scale;
        }
    }

    private void UpdateAmmoUI()
    {
        if (_player != null)
        {
            _scale.x = ((ShipController)_player).bulletAmmo / ((ShipController)_player).bulletAmmoMax;
            _scale.y = _scale.z = 1;
            ammoUI.transform.localScale = _scale;
        }
    }

    private IEnumerator UpdateUI()
    {
        while (true)
        {
            while (GameManager.I.clock.timeScale == 0)
            {
                yield return null;
            }
            UpdateHealthUI();
            UpdateAmmoUI();
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void ApplyFade(Graphic graphic, Color start, Color end, float duration, bool mirror)
    {
        if (!_fadingGraphics.Contains(graphic))
        {
            StartCoroutine(Fade(graphic, start, end, duration, mirror));
        }
    }

    private IEnumerator Fade(Graphic graphic, Color start, Color end, float duration, bool mirror)
    {
        while (!GameManager.I.initialized)
        {
            yield return null;
        }
        _fadingGraphics.Add(graphic);
        float startTime = GameManager.I.clock.totalSeconds;
        float adjustedDuration = duration;
        if (mirror)
        {
            adjustedDuration = duration / 2;
        }
        while (graphic.color != end)
        {
            graphic.color = Color.Lerp(start, end, (GameManager.I.clock.totalSeconds - startTime) / adjustedDuration);
            yield return null;
        }
        _fadingGraphics.Remove(graphic);
        if (mirror)
        {
            StartCoroutine(Fade(graphic, end, start, adjustedDuration, false));
        }
    }
}
