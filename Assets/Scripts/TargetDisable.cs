﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetDisable : MonoBehaviour {

    public Projectile projectile;

    private void Start()
    {
        if (projectile == null && transform.parent != null)
        {
            projectile = transform.parent.GetComponent<Projectile>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (projectile is Missile)
        {
            Missile m = (Missile)projectile;
            if (other.transform.GetComponent<EntityController>() == m.targetEntity)
            {
                m.targetEntity = null;
            }
        }
    }
}
