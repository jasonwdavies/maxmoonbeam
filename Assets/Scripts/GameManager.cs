﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _I;
    public static GameManager I
    {
        get
        {
            if (_I == null)
            {
                _I = FindObjectOfType<GameManager>();
            }
            return _I;
        }
    }
    [HideInInspector] public bool initialized;
    [HideInInspector] public Clock clock;

    public float sceneFadeTime;
    public Color damageTaken;
    public RailcarController playerRailcar;

    [Header("Prefabs")]
    public RailcarController[] railcarEnemies;
    public RailcarController[] railcarAsteroids;
    public GameObject bullet;
    public GameObject missile;
    public GameObject mine;

    private void Start()
    {
        clock = GetComponentInChildren<Clock>();
        initialized = true;
    }

    public void RestartCurrentScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
