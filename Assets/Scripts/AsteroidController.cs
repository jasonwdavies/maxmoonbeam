﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : EntityController {

    [Header("Asteroid")]
    public float rotationSpeed;
    public float initialForce;

    private Vector3 _rotationDirection;

    private new void Start()
    {
        base.Start();
        _rotationDirection = new Vector3(Random.Range(-rotationSpeed, rotationSpeed), 
                                         Random.Range(-rotationSpeed, rotationSpeed), 
                                         Random.Range(-rotationSpeed, rotationSpeed));
        _rb.AddForce(new Vector3(Random.Range(-initialForce, initialForce),
                                 Random.Range(-initialForce, initialForce),
                                 Random.Range(-initialForce, initialForce)));
        _initialized = true;
    }

    private void FixedUpdate()
    {
        //transform.Translate(movementDirection.normalized * GameManager.I.clock.timeScale * Time.deltaTime);
        transform.Rotate(_rotationDirection * GameManager.I.clock.timeScale * Time.deltaTime);
    }

    private new void Update()
    {
        base.Update();
    }

    private new void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
    }
}
