﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Material skybox;
    public bool trail;
    public float trailSpeed;
    public float xBound;
    public float yBound;
    public float zTargetOffset;
    private Transform _target;
    private Vector3 _targetPos;
    private Vector3 _newPos;

    private void Start()
    {
        RenderSettings.skybox = skybox;
        _target = GameManager.I.playerRailcar.GetComponentInChildren<EntityController>().transform;
        _newPos.z = zTargetOffset;
        transform.localPosition = _newPos;
    }

    private void FixedUpdate ()
    {
        if (trail)
        {
            if (_target.localPosition.x > xBound || _target.localPosition.x < -xBound
                || _target.localPosition.y > yBound || _target.localPosition.y < -yBound)
            {
                _targetPos = Vector3.MoveTowards(transform.localPosition, _target.localPosition, Mathf.Abs(trailSpeed * 100 * Time.deltaTime));
                _newPos.x = _targetPos.x;
                _newPos.y = _targetPos.y;
                transform.localPosition = _newPos;
            }
        }
	}
}
