﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : EntityController
{
    [Header("Ship")]
    public ParticleSystem fxFuel;
    public float shiftSpeed;
    public float shiftSpeedEvadeScale;
    public float railSpeedEvadeScale;
    public float attackCooldown;

    public float evadeRotations;
    public float evadeRotSpeed;
    public float evadeCooldown;

    public bool evading { get { return _evading; } }

    public Transform[] bulletPos;
    public float bulletAmmo;
    public float bulletAmmoMax;
    public float bulletRegen;
    public float bulletRegenDelay;
    public float bulletDamage;
    public float bulletSpeed;
    public float bulletDuration;
    public float bulletLength;
    public float bulletWidth;
    public Color bulletColor;
    public Color bulletGlowColor;

    public Transform[] missilePos;
    public bool missileTarget;
    public float missileAmmo;
    public float missileAmmoMax;
    public float missileRegen;
    public float missileRegenDelay;
    public float missileDamage;
    public float missileSpeed;
    public float missileDuration;

    public Transform[] minePos;
    public bool mineTarget;
    public float mineAmmo;
    public float mineAmmoMax;
    public float mineRegen;
    public float mineRegenDelay;
    public float mineDamage;
    public float mineSpeed;
    public float mineDuration;

    protected float _lastEvadeTime;
    protected float _lastAttackTime;
    protected float _originalShiftSpeed;
    //protected float _originalMovementSpeedScale;
    protected bool _evading;

    private float _prevBulletAmmo;
    private float _timeOfLastShot;

    protected new void Start()
    {
        base.Start();
        _lastEvadeTime = -evadeCooldown; // offset to allow for immediate application
        //_lastAttackTime = -attackCooldown; // offset to allow for immediate application
        _originalShiftSpeed = shiftSpeed;
    }

    protected new void Update()
    {
        if (health > 0)
        {
            // Health regenerates after a you take damage and a delay has occured
            if (bulletAmmo < _prevBulletAmmo)
            {
                _timeOfLastShot = GameManager.I.clock.totalSeconds;
            }
            else if (bulletAmmo < healthMax && GameManager.I.clock.totalSeconds >= _timeOfLastShot + bulletRegenDelay)
            {
                bulletAmmo = Mathf.Min(bulletAmmo + bulletRegen, bulletAmmoMax);
            }
            _prevBulletAmmo = bulletAmmo;
        }
        else if (health <= 0 && !_dead)
        {
            if (fxFuel != null)
            {
                fxFuel.Stop();
            }
        }
        base.Update();
    }

    protected void Attack()
    {
        Attack(bulletDamage, bulletSpeed, bulletDuration, bulletLength, bulletWidth, bulletColor, bulletGlowColor);
    }

    protected void Attack(float damage, float speed, float duration, float length, float width, Color color, Color glowColor)
    {
        GameObject obj;
        _lastAttackTime = GameManager.I.clock.totalSeconds;
        for (int i = 0; i < bulletPos.Length; i++)
        {
            if (bulletAmmo > 0)
            {
                bulletAmmo -= 1;
                obj = Instantiate(GameManager.I.bullet);
                obj.transform.position = bulletPos[i].position;
                Bullet bullet = obj.GetComponent<Bullet>();
                bullet.damage = damage;
                bullet.speed = speed;
                bullet.duration = duration;
                bullet.length = length;
                bullet.width = width;
                bullet.lineColor = color;
                bullet.glowColor = glowColor;
                bullet.direction = bulletPos[i].forward * length;
            }
        }

        for (int i = 0; i < missilePos.Length; i++)
        {
            if (missileAmmo > 0)
            {
                missileAmmo -= 1;
                obj = Instantiate(GameManager.I.missile);
                obj.transform.position = missilePos[i].position;
                Missile missile = obj.GetComponent<Missile>();
                missile.damage = missileDamage;
                missile.duration = missileDuration;
                if (transform.CompareTag("Enemy"))
                {
                    missile.targetEntity = GameManager.I.playerRailcar.entities[0];
                    missile.speed = missileSpeed;
                    missile.targetCooldown = ((EnemyController)this).targetCooldown;
                }
                else if (RailcarManager.I.enemyRailcars.Count > 0)
                {
                    missile.targetEntity = RailcarManager.I.enemyRailcars[Random.Range(0, RailcarManager.I.enemyRailcars.Count)].entities[0];
                    missile.speed = missileSpeed;
                }
            }
        }

        for (int i = 0; i < minePos.Length; i++)
        {
            if (mineAmmo > 0)
            {
                mineAmmo -= 1;
                obj = Instantiate(GameManager.I.mine);
                obj.transform.position = minePos[i].position;
                Mine mine = obj.GetComponent<Mine>();
                mine.damage = mineDamage;
                mine.duration = mineDuration;
                if (transform.CompareTag("Enemy"))
                {
                    mine.targetEntity = GameManager.I.playerRailcar.entities[0];
                    mine.speed = mineSpeed;
                    mine.targetCooldown = ((EnemyController)this).targetCooldown;
                }
            }
        }
    }

    protected IEnumerator Evade()
    {
        Vector3 _originalRot = transform.localEulerAngles;
        Vector3 _currentRot = _originalRot;
        if (!evading)
        {
            _evading = true;
            shiftSpeed *= shiftSpeedEvadeScale;
            railSpeed *= railSpeedEvadeScale;
            float evadeRotTotal = 0;
            float evadeRotChange = 0;
            while (evadeRotTotal < evadeRotations * 360)
            {
                evadeRotChange = evadeRotSpeed * 100 * Time.deltaTime * GameManager.I.clock.timeScale;
                evadeRotTotal += Mathf.Abs(evadeRotChange);
                _currentRot.z += evadeRotChange;
                transform.localEulerAngles = _currentRot;
                yield return new WaitForSeconds(0.01f);
            }
            //shiftSpeed = _originalShiftSpeed;
            //railSpeed = _originalRailSpeed;
            _evading = false;
        }
        transform.localEulerAngles = _originalRot; // reset
    }
}
