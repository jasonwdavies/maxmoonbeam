﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// USEFUL: http://rossum.sourceforge.net/papers/CalculationsForRobotics/CirclePath.htm

public class RailcarManager : MonoBehaviour
{
    private static RailcarManager _I;
    public static RailcarManager I
    {
        get
        {
            if (_I == null)
            {
                _I = FindObjectOfType<RailcarManager>();
            }
            return _I;
        }
    }

    public float objectiveUpdateCooldown;
    public float targetDistance;
    public float avoidDistance;
    public float destroyDistance;

    public float railDuration;
    public float spawnIntervalShip;
    public float spawnIntervalAsteroidField;
    //public float spawnIntervalRandOffset;
    public float spawnIntervalOffset;
    public float amplitudeMin;
    public float amplitudeMax;
    public float periodMin;
    public float periodMax;
    public float radiusMin;
    public float radiusMax;
    //public float spiralCompressionMin;
    //public float spiralCompressionMax;

    private enum Equation { linear, wave, circle, spiral }
    private Equation _equation;
    private float _coefficientAmplitude = 40;
    private float _coefficientPeriod = 1;
    private float _radius = 5;
    //private float _spiralCompressionScale = 1;

    private RailcarController _playerRailcar;
    public List<RailcarController> enemyRailcars { get { return _enemyRailcars; } }
    private List<RailcarController> _enemyRailcars;
    private List<RailcarController> _enemyRailcarsRemoval;
    private float _lastObjectiveUpdate;
    private float _newSpawnInterval;

    private bool _completedRail;

    private void Start ()
    {
        _playerRailcar = GameManager.I.playerRailcar;
        _enemyRailcars = new List<RailcarController>();
        _enemyRailcarsRemoval = new List<RailcarController>();

        // Generate Rail
        SetEquation(Equation.linear);
    }

    private void Update()
    {
        if ((_playerRailcar.currentDistance >= railDuration) && !_completedRail)
        {
            _completedRail = true;
            UIManager.I.ApplyFade(UIManager.I.vision, Color.clear, Color.white, GameManager.I.sceneFadeTime, false);
            UIManager.I.text.text = "COMPLETED!";
            UIManager.I.ApplyFade(UIManager.I.text, Color.clear, Color.black, GameManager.I.sceneFadeTime / 2, true);
            // TODO: Delay the pause until after the fade is complete!
        }
        else if ((_playerRailcar.currentDistance > _newSpawnInterval) && !_completedRail)
        {
            //_newSpawnInterval += spawnInterval + Random.Range(-spawnIntervalRandOffset, spawnIntervalRandOffset);
            RailcarController railcar = null;
            switch (Random.Range(0, 1)) // enemy ship vs asteroid field
            {
                case 0: // enemy ship
                    _newSpawnInterval += spawnIntervalShip;
                    railcar = Instantiate(GameManager.I.railcarEnemies[Random.Range(0, GameManager.I.railcarEnemies.Length)]);
                    railcar.currentDistance = _newSpawnInterval + spawnIntervalOffset;
                    _enemyRailcars.Add(railcar);
                    break;
                case 1: // asteroid fields
                    _newSpawnInterval += spawnIntervalAsteroidField;
                    railcar = Instantiate(GameManager.I.railcarAsteroids[Random.Range(0, GameManager.I.railcarAsteroids.Length)]);
                    railcar.transform.localEulerAngles = new Vector3(0, 0, Random.Range(0, 180));
                    railcar.currentDistance = _newSpawnInterval + spawnIntervalOffset;
                    _enemyRailcars.Add(railcar);
                    break;
            }
        }

        if (GameManager.I.clock.totalSeconds > _lastObjectiveUpdate + objectiveUpdateCooldown)
        {
            _lastObjectiveUpdate = GameManager.I.clock.totalSeconds;
            foreach (RailcarController railcar in _enemyRailcars)
            {
                if (_playerRailcar.currentDistance + destroyDistance >= railcar.currentDistance)
                {
                    _enemyRailcarsRemoval.Add(railcar);
                }
                else
                {
                    for (int i=0; i<railcar.entities.Length; i++)
                    {
                        if (railcar.entities[i] is EnemyController 
                                && ((EnemyController)railcar.entities[i]).m_objective == EnemyController.objective.target
                                && _playerRailcar.currentDistance + avoidDistance >= railcar.currentDistance)
                            ((EnemyController)railcar.entities[i]).m_objective = EnemyController.objective.avoid;
                    }
                }
            }
            // Removal of railcars that have passed the player
            for (int i=0; i<_enemyRailcarsRemoval.Count; i++)
            {
                _enemyRailcars.Remove(_enemyRailcarsRemoval[i]);
                Destroy(_enemyRailcarsRemoval[i].gameObject);
            }
            _enemyRailcarsRemoval.Clear();
        }
    }

    private void SetEquation()
    {
        _equation = (Equation)(Random.Range(0, System.Enum.GetNames(typeof(Equation)).Length));
        //_coefficient = Random.Range(coefficientMin, coefficientMax);
    }

    private void SetEquation(Equation e)
    {
        _equation = e;
        //_coefficient = Random.Range(coefficientMin, coefficientMax);
        //Debug.Log("_coefficient is " + _coefficient);
        _coefficientAmplitude = Random.Range(amplitudeMin, amplitudeMax);
        //Debug.Log("_coefficientAmplitude is " + _coefficientAmplitude);
        _coefficientPeriod = Random.Range(periodMin, periodMax);
        //Debug.Log("_coefficientPeriod is " + _coefficientPeriod);
    }

    public Vector3 GetPosOnRail(float time)
    {
        switch (_equation)
        {
            default:
            case Equation.linear:
                return new Vector3(0, 0 * time, time);
            case Equation.wave:
                return new Vector3(0, _coefficientAmplitude * Mathf.Sin(_coefficientPeriod * time), time);
            case Equation.circle: // r^2 = x^2 + y^2
                float y = Mathf.Pow(_radius, 2) - Mathf.Pow(time, 2);
                if (y < 0)
                {
                    y = -1 * Mathf.Sqrt(Mathf.Abs(y));
                }
                else
                {
                    y = Mathf.Sqrt(y);
                }
                float z = Mathf.Pow(_radius, 2) - Mathf.Pow(time, 2);
                if (z < 0)
                {
                    z = -1 * Mathf.Sqrt(Mathf.Abs(z));
                }
                else
                {
                    z = Mathf.Sqrt(z);
                }
                return new Vector3(0, y, z);
        }
    }
}
