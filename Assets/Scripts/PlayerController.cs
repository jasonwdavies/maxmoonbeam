﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : ShipController
{
    [Header("Player")]
    public SpriteRenderer[] cursorSprites;
    public float avoidBulletTimeScale;
    public float avoidBulletTimeDuration;
    public float chargeReqTime;
    public float chargeCost;
    public float chargeDamage;
    public float chargeBulletSpeed;
    public float chargeLength;
    public float chargeWidth;
    public float chargeImpactWidthScale;
    public Color chargeColor;
    public Color chargeGlowColor;
    public float xRotLimit;
    public float yRotLimit;
    public float zRotLimit;
    public float xRotSpeed;
    public float yRotSpeed;
    public float zRotSpeed;

    private Vector3 _newPos;
    private Vector3 _newRot;
    private float _chargeStartTime;

    private new void Start()
    {
        base.Start();
        _initialized = true;
    }
    
    private void FixedUpdate ()
    {
        if (health > 0)
        {
            //if (evading)
            //{
            //    shiftSpeedEvadeScale = _originalShiftSpeed;
            //    movementSpeedEvadeScale = _originalMovementSpeed;
            //}
            //else
            //{
            //    shiftSpeedEvadeScale = 1;
            //}
            _newPos = transform.localPosition;
            _newRot = transform.localEulerAngles;
            _newRot.x = Mathf.Round(_newRot.x);
            _newRot.y = Mathf.Round(_newRot.y);
            _newRot.z = Mathf.Round(_newRot.z);
            if (_newRot.x > 180)
            {
                _newRot.x -= 360;
            }
            if (_newRot.y > 180)
            {
                _newRot.y -= 360;
            }
            if (_newRot.z > 180)
            {
                _newRot.z -= 360;
            }

            if (Input.GetKey(KeyCode.W))
            {
                _newPos.y += shiftSpeed * GameManager.I.clock.timeScale;
                _newRot.x -= xRotSpeed * GameManager.I.clock.timeScale;
            }
            else if (_newRot.x < 0)
            {
                _newRot.x += xRotSpeed * GameManager.I.clock.timeScale;
            }

            if (Input.GetKey(KeyCode.S))
            {
                _newPos.y -= shiftSpeed * GameManager.I.clock.timeScale;
                _newRot.x += xRotSpeed * GameManager.I.clock.timeScale;
            }
            else if (_newRot.x > 0)
            {
                _newRot.x -= xRotSpeed * GameManager.I.clock.timeScale;
            }

            if (Input.GetKey(KeyCode.A))
            {
                if (evadeRotSpeed < 0)
                {
                    evadeRotSpeed *= -1;
                }
                _newPos.x -= shiftSpeed * shiftSpeedEvadeScale * GameManager.I.clock.timeScale;
                _newRot.y -= yRotSpeed * GameManager.I.clock.timeScale;
                _newRot.z += zRotSpeed * GameManager.I.clock.timeScale;
            }
            else if (_newRot.y < 0)
            {
                _newRot.y += yRotSpeed * GameManager.I.clock.timeScale;
                _newRot.z -= zRotSpeed * GameManager.I.clock.timeScale;
            }

            if (Input.GetKey(KeyCode.D))
            {
                if (evadeRotSpeed > 0)
                {
                    evadeRotSpeed *= -1;
                }
                _newPos.x += shiftSpeed * shiftSpeedEvadeScale * GameManager.I.clock.timeScale;
                _newRot.y += yRotSpeed * GameManager.I.clock.timeScale;
                _newRot.z -= zRotSpeed * GameManager.I.clock.timeScale;
            }
            else if (_newRot.y > 0)
            {
                _newRot.y -= yRotSpeed * GameManager.I.clock.timeScale;
                _newRot.z += zRotSpeed * GameManager.I.clock.timeScale;
            }

            _newPos.x = Mathf.Clamp(_newPos.x, -xPosLimit, xPosLimit);
            _newPos.y = Mathf.Clamp(_newPos.y, -yPosLimit, yPosLimit);
            transform.localPosition = _newPos;

            if (!evading)
            {
                _newRot.x = Mathf.Clamp(_newRot.x, -xRotLimit, xRotLimit);
                _newRot.y = Mathf.Clamp(_newRot.y, -yRotLimit, yRotLimit);
                _newRot.z = Mathf.Clamp(_newRot.z, -zRotLimit, zRotLimit);
                transform.localEulerAngles = _newRot;
            }
        }
    }

    private new void Update()
    {
        if (health > 0)
        {
            bulletAmmo = Mathf.Min(bulletAmmoMax, bulletAmmo + bulletRegen);
            if (GameManager.I.clock.totalSeconds >= _lastAttackTime + attackCooldown)
            {
                if (Physics.Raycast(transform.position + transform.forward, transform.forward * 100, 300, targetMask))
                {
                    for (int i=0; i<cursorSprites.Length; i++)
                    {
                        cursorSprites[i].color = Color.yellow;
                    }
                }
                else
                {
                    for (int i = 0; i < cursorSprites.Length; i++)
                    {
                        cursorSprites[i].color = Color.blue;
                    }
                }

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    _chargeStartTime = GameManager.I.clock.totalSeconds;
                }
                else if (Input.GetKeyUp(KeyCode.Space))
                {
                    if (GameManager.I.clock.totalSeconds - _chargeStartTime >= chargeReqTime && bulletAmmo >= chargeCost)
                    {
                        Attack(chargeDamage, chargeBulletSpeed, bulletDuration, chargeLength, chargeWidth, chargeColor, chargeGlowColor);
                    }
                    else if (bulletAmmo >= 1)
                    {
                        Attack();
                    }
                }
            }
            // Spin evade
            if (GameManager.I.clock.totalSeconds >= _lastEvadeTime + evadeCooldown)
            {
                if (Input.GetKeyDown(KeyCode.LeftShift))
                {
                    _lastEvadeTime = GameManager.I.clock.totalSeconds;
                    StartCoroutine(Evade());
                }
            }
            // Release the speed increase scale from boosting
            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                shiftSpeed = _originalShiftSpeed;
                railSpeed = _originalRailSpeed;
            }
        }
        base.Update();
    }
}