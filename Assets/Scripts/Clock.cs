﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Clock : MonoBehaviour
{
    public float totalSeconds { get { return _totalSeconds; } }
    public float timeScale { get { return _timeScale; } }

    private float _totalSeconds;
    private float _timeScale;
    private bool _paused;
    private bool _scaled;

    void Start()
    {
        _timeScale = 1;
    }

    void Update()
    {
        _totalSeconds += _timeScale * Time.deltaTime;
    }

    public void Pause()
    {
        _paused = true;
        _timeScale = 0;
    }

    public void Unpause()
    {
        _paused = false;
        _timeScale = 1;
    }

    public void SetTimeScale(float scale, float duration)
    {
        if (!_paused && !_scaled)
        {
            StartCoroutine(TimeScale(scale, duration));
        }
    }

    private IEnumerator TimeScale(float scale, float duration)
    {
        float _originalTimeScale = _timeScale;
        _scaled = true;
        _timeScale = scale;
        //yield return new WaitForSeconds(duration);
        float durationCounter = 0;
        float startTime = _totalSeconds;
        while (durationCounter < (duration * scale))
        {
            durationCounter = _totalSeconds - startTime;
            yield return null;
        }
        _timeScale = _originalTimeScale;
        _scaled = false;
    }
}