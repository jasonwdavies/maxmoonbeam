﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : ShipController
{
    [Header("Enemy")]
    public float targetCooldown;
    public enum objective { none, avoid, target, cruise, circle };
    [HideInInspector] public objective m_objective;

    protected EntityController _targetEntity;

    private float _lastTarget;
    private Vector3 _targetPos;
    private Vector3 _newPos;

    protected new void Start()
    {
        base.Start();
        _targetEntity = GameManager.I.playerRailcar.GetComponentInChildren<EntityController>();
        _initialized = true;
    }

    protected new void Update()
    {
        if (health > 0)
        {
            if (GameManager.I.clock.totalSeconds >= _lastAttackTime + attackCooldown 
                && (m_objective != objective.none || m_objective != objective.avoid))
            {
                Attack();
            }
        }
        base.Update();
    }

    protected void FixedUpdate()
    {
        if (health > 0)
        {
            switch (m_objective)
            {
                case objective.none:
                case objective.avoid:
                    break;
                case objective.target:
                    if (GameManager.I.clock.totalSeconds >= _lastTarget + targetCooldown)
                    {
                        _lastTarget = GameManager.I.clock.totalSeconds;
                        _targetPos = _targetEntity.transform.localPosition;
                        _targetPos.x *= -1;
                    }
                    if (transform.localPosition != _targetPos)
                    {
                        _newPos = Vector3.MoveTowards(transform.localPosition, _targetPos, Mathf.Abs(shiftSpeed * 100 * Time.deltaTime * GameManager.I.clock.timeScale));
                        transform.localPosition = _newPos;
                    }
                    transform.LookAt(_targetEntity.transform);
                    break;
                case objective.cruise:
                    _targetPos = transform.forward * 2000;
                    _targetPos.z = 0;
                    _newPos = Vector3.MoveTowards(transform.position, _targetPos, Mathf.Abs(shiftSpeed * 100 * Time.deltaTime * GameManager.I.clock.timeScale));
                    transform.position = _newPos;
                    break;
                case objective.circle:
                    break;
            }
        }
    }
}
