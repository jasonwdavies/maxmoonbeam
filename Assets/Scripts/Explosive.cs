﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosive : Projectile {

    public float targetCooldown;
    public EntityController targetEntity;

    private float _lastTarget;

    private new void Start()
    {
        base.Start();
    }

    private new void FixedUpdate()
    {
        if (targetEntity != null)
        {
            if (GameManager.I.clock.totalSeconds >= _lastTarget + targetCooldown)
            {
                _lastTarget = GameManager.I.clock.totalSeconds;
                direction = targetEntity.transform.localPosition - transform.position;
            }
            transform.LookAt(targetEntity.transform);
        }
        base.FixedUpdate();
    }

    private new void Update()
    {
        base.Update();
    }
}
